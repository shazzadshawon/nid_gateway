<?php
	require_once ("config.php");
	session_start();
?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Login Page - NID Gateway</title>
		<meta name="description" content="User login page" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
        <link rel="shortcut icon" href="<?php echo $baseAddress;?>assets/img/bd_logo.png">
		<link rel="stylesheet" type="text/css" href="<?php echo $baseAddress;?>assets/lib/tether/css/tether.min.css">
		<link rel="stylesheet" type="text/css" href="<?php echo $baseAddress;?>assets/lib/tether/css/tether-theme-arrows.min.css">
		<link rel="stylesheet" href="<?php echo $baseAddress;?>assets/css/bootstrap.css" />
		<link rel="stylesheet" href="<?php echo $baseAddress;?>assets/css/font-awesome.css" />
		<link rel="stylesheet" href="<?php echo $baseAddress;?>assets/css/ace-fonts.css" />
		<link rel="stylesheet" href="<?php echo $baseAddress;?>assets/css/ace.css" />
		<link rel="stylesheet" href="<?php echo $baseAddress;?>assets/css/jquery.gritter.css" />
        <link rel="stylesheet" href="<?php echo $baseAddress;?>assets/css/ace-part2.css" />
		<link rel="stylesheet" href="<?php echo $baseAddress;?>assets/css/ace-rtl.css" />
		<link rel="stylesheet" type="text/css" href="<?php echo $baseAddress;?>assets/lib/bootstrap-datetimepicker/bootstrap-datetimepicker.min.css">
        <link rel="stylesheet" href="<?php echo $baseAddress;?>assets/css/ace-ie.css" />
    </head>

	<body class="login-layout light-login">
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<img src="assets/img/bd_logo.png" width="100" style="margin-top: 20px" />
								<h1>
									<span class="grey" id="id-text2">NID Gateway</span>
								</h1>
								<h4 class="blue" id="id-company-text">Government of the People's Republic of Bangladesh</h4>
							</div>

							<div class="space-6"></div>

							<div class="position-relative">
								<div id="login-box" class="login-box visible widget-box no-border">
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header blue lighter bigger">
												<i class="ace-icon fa fa-coffee green"></i>
												Please Enter Your Information
											</h4>

											<div class="space-6"></div>

											<form>
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input id="username" type="text" class="form-control" placeholder="Enter Your Username"/>
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input id="password" type="password" class="form-control" placeholder="Password" maxlength="20" />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<div class="space"></div>

													<div class="clearfix">

														<!-- <button type="button" class="pull-right btn btn-lg btn-success"
                                                                onclick="login();"
                                                                > -->
                                                        	<button type="button" class="btn btn-success btn-border" style="width:100%;" onclick="login();">
                                                        		<i class="ace-icon fa fa-key"></i>
																<span class="bigger-110">Login</span>
															</button>
														<!-- </button> -->
													</div>

													<div class="space-4"></div>
												</fieldset>
											</form>
										</div><!-- /.widget-main -->
										<div class="toolbar clearfix">
											<div class="pull-right">
												<a href="#" data-target="#signup-box" class="user-signup-link">
													User Registration
													<i class="ace-icon fa fa-arrow-right"></i>
												</a>
											</div>
										</div>

									</div><!-- /.widget-body -->
								</div><!-- /.login-box -->
									<p id="login_success" class="alert alert-success" hidden='true'>Matched! Logging-In.  Please be Paitent ..</p>
									<p id="login_error" class="alert alert-danger" hidden="true"></p>
									<p id="login_wait" hidden="true" style="background-color: rgba(255, 255, 255, 0.1)"><img src="assets/img/please_wait.gif"></p>
								<div id="signup-box" class="signup-box widget-box no-border" style="margin-bottom: 30px">
                                    <div id="usr_reg_req_err" class="toolbar center" style="background: transparent; color: red; display: none" ></div>
									<div class="widget-body">
										<div class="widget-main">
											<h4 class="header green lighter bigger">
												<i class="ace-icon fa fa-users blue"></i>
												New User Registration
											</h4>

											<div class="space-6"></div>
											<p> Enter your details to begin: </p>

											<form>
												<fieldset>
													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input id="usr_reg_req-email" type="email" class="form-control" placeholder="Email" required />
															<i class="ace-icon fa fa-envelope"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input id="usr_reg_req-name" type="text" class="form-control" placeholder="Username" required />
															<i class="ace-icon fa fa-user"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input id="usr_reg_req-password" type="password" class="form-control" placeholder="Password" required />
															<i class="ace-icon fa fa-lock"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input id="usr_reg_req-password_re" type="password" class="form-control" placeholder="Repeat password" required />
															<i class="ace-icon fa fa-retweet"></i>
														</span>
													</label>

													<label class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input id="usr_reg_req-phoneNo" type="text" class="form-control" placeholder="Phone No" required />
															<i class="ace-icon fa fa-phone"></i>
														</span>
													</label>

													<label id="usr_reg_nid" class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input id="usr_reg_req-nid" type="text" class="form-control" placeholder="NID Number" required />
															<i class="ace-icon fa fa-id-badge"></i>
														</span>
													</label>

                                                    <label id="usr_reg_dob" class="block clearfix">
														<span class="block input-icon input-icon-right">
<!--															<input id="usr_reg_req-dob" type="text" class="form-control reg_req_datepicker" name="datepicker" placeholder="Date of Birth" required />-->
															<div class="input-append date reg_req_datepicker">
																<input id="usr_reg_req-dob" class="form-control w70" size="16" type="text" value="" readonly placeholder="Date of Birth">
																<span class="add-on"><i class="icon-th" aria-hidden="true"></i></span>
															</div>
															<i class="ace-icon fa fa-calendar"></i>
														</span>
                                                    </label>

													<label id="usr_reg_pass" class="block clearfix">
														<span class="block input-icon input-icon-right">
															<input id="usr_reg_req-passport" type="text" class="form-control" placeholder="Passport" />
														</span>
													</label>

													<label id="choose_nid_passport" class="block">
														<input id="usr_reg_req-nidorpassport" type="checkbox" class="ace"/>
														<span class="lbl">
															<span>Choose NID / Passport</span>
														</span>
													</label>

													<label class="block">
														<input id="usr_reg_req-lisence" type="checkbox" class="ace" required />
														<span class="lbl">
															I accept the
															<a href="#">User Agreement</a>
														</span>
													</label>

													<div class="space-24"></div>

													<div class="clearfix">
														<button type="reset" class="width-30 pull-left btn btn-sm">
															<i class="ace-icon fa fa-refresh"></i>
															<span class="bigger-110">Reset</span>
														</button>

														<button id="user_register_req" type="button" class="width-65 pull-right btn btn-sm btn-success">
															<span class="bigger-110">Register</span>

															<i class="ace-icon fa fa-arrow-right icon-on-right"></i>
														</button>
													</div>
												</fieldset>
											</form>
										</div>

										<div class="toolbar center">
											<a id="back_to_login" href="#" data-target="#login-box" class="back-to-login-link">
												<i class="ace-icon fa fa-arrow-left"></i>
												Back to login
											</a>
										</div>
									</div><!-- /.widget-body -->
								</div><!-- /.signup-box -->
							</div><!-- /.position-relative -->
						</div>
					</div><!-- /.col -->
				</div><!-- /.row -->
			</div><!-- /.main-content -->
		</div><!-- /.main-container -->

		<script type="text/javascript">
			if('ontouchstart' in document.documentElement) document.write("<script src='<?php echo $baseAddress;?>assets/js/jquery.mobile.custom.js'>"+"<"+"/script>");
		</script>
		<script type="text/javascript" src="<?php echo $baseAddress;?>assets/lib/tether/js/tether.min.js"></script>
		<script src="<?php echo $baseAddress;?>assets/js/jquery_custom.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseAddress;?>assets/js/bootstrap_custome.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseAddress;?>assets/lib/moment/min/moment-with-locales.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseAddress;?>assets/lib/moment/min/moment-timezone-with-data-2010-2020.min.js"></script>
		<script type="text/javascript" src="<?php echo $baseAddress;?>assets/js/all_login.min.js"></script>

		<script type="text/javascript">
			var login_api = "http://"+config['apiaddr']+"/login";
			var url = 'http://'+config['host'];
			APP_SERVER_BASE = url;
			ADMIN_BASE = APP_SERVER_BASE + '<?php echo $baseAdd_ext; ?>';
			APP_BASE_API = ADMIN_BASE + '/api';
			APP_BASE_VIEW = ADMIN_BASE + '/views';

			function gritter(title, text, sticky, time, class_name){
				$.gritter.add({
					title: title,
					text: text,
					sticky: sticky,
					time: time,
					class_name: class_name
				});
				return false;
			}

			function newUser(){
		    	bootbox.dialog({
					title: "Registration Form",
					message: "<p>You can not Register without any Valid account</p> <p>If you dont have any account contact with Administration</p>"
				});
			}

			function validate(node){
				if ( $.trim(node.val()) == '' ){
					return false;
				} else {
					return true;
				}
			}

            function login(){
            	if(!validate($('#username')) || !validate($('#password')) ){
					$('#login_wait').hide();
					$('#login_success').show().html("Both Inputs are Mandatory !").delay(5000).fadeOut();
					$('#login_error').hide();
					return;
				}
				$('#login_wait').show();
                var name = $('#username').val();
                var password = $('#password').val();
                var data = {'name':name,'password':password};

                $.ajax({
                    url : login_api,
					beforeSend: function( xhr ) {
						xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
					},
                    type: 'POST',
                    processData: false,
					crossDomain: true,
                    data: JSON.stringify(data),
                    success:function(data, textStatus, jqXHR) {
						$('#login_wait').hide();
                        var lData = JSON.parse(data);
                        console.log(lData.isSuccess);
                        if(lData && lData.isSuccess){
                        	$('#login_success').show().html("Login Success .. Redirrecting Now ...");
                        	$('#login_error').hide();
							var loginsavedata = {'status':lData.isSuccess,'token':lData.message};
							$.ajax({
								url: APP_BASE_API+'/login',
								type: 'POST',
								processData: false,
								data: JSON.stringify(loginsavedata),
								success: function(data, textStatus, jqXHR){
									if(data.status == "success"){
										window.location = ADMIN_BASE;
									}
								},
								error: function(data, textStatus, jqXHR){
									$('#login_success').show().html("Application API unreachable");
								}
							});
                        } else {
                        	$('#login_success').hide();
                        	$('#login_error').html(lData.message).show().delay(5000).fadeOut();
                        }
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
						$('#login_wait').hide();
						$('#login_success').hide();
						$('#login_error').html("SERVER OFFLINE").show().delay(5000).fadeOut();
                    },
					timeout: 10000
                }) .done(function(data) {

				});
            }

            var form_data = new Object();
            var usr_req_form_send = new Object();

			function requrireMsg(){
				$('#usr_reg_req_err').html('');
				$('#usr_reg_req_err').show().html("All Fields are required");
				gritter('All Fields are required', 'Please fill up all fields', false, '2500', 'gritter-error');
			}
			function nameMsg(){
				$('#usr_reg_req_err').html('');
				$('#usr_reg_req_err').show().html("The name you entered "+form_data.name+" is alredy taken by others.");
				gritter('Name Fault', "The name you entered "+form_data.name+" is alredy taken by others.", false, '2500', 'gritter-error');
			}
			function serverMsg(){
				$('#usr_reg_req_err').html('');
				$('#usr_reg_req_err').show().html("SERVER ERROR");
				gritter('Server Error', 'Server Error', false, '2500', 'gritter-error');
			}
			function lisenceMsg(){
				$('#usr_reg_req_err').html('');
				$('#usr_reg_req_err').show().html("Lisence Agreement must be accepted");
				gritter('Lisence', 'Lisence Agreement must be accepted', false, '2500', 'gritter-error');
			}
			function passMsg(){
				$('#usr_reg_req_err').html('');
				$('#usr_reg_req_err').show().html("Your Entered Password does not match");
				gritter('Password mismatch', 'Your Entered Password does not match', false, '2500', 'gritter-error');
			}
			function invalidEmailMsg(){
				$('#usr_reg_req_err').html('');
				$('#usr_reg_req_err').show().html("Your Entered Email Address is not Valid");
				gritter('Email Error', 'Your Entered Email Address is not Valid', false, '2500', 'gritter-error');
			}
			function nidlengthMsg(){
				$('#usr_reg_req_err').html('');
				$('#usr_reg_req_err').show().html("NID number must be either 17 or 13 character long");
				gritter('NID Error', 'NID number must be either 17 or 13 character long', false, '2500', 'gritter-error');
			}

			function isValidEmailAddress(emailAddress) {
				var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
				return pattern.test(emailAddress);
			}

			function usr_reg_form_collect(){
				form_data.email = $('#usr_reg_req-email').val();
				form_data.name = $('#usr_reg_req-name').val();
				form_data.password = $('#usr_reg_req-password').val();
				form_data.re_password = $('#usr_reg_req-password_re').val();
				form_data.phoneNo = $('#usr_reg_req-phoneNo').val();
				form_data.nidorpassport = $('#usr_reg_req-nidorpassport').prop('checked');
				form_data.nidNo = $('#usr_reg_req-nid').val();
				form_data.dob = $('#usr_reg_req-dob').val();
				form_data.passportNo = $('#usr_reg_req-passport').val();
				form_data.lisence = $('#usr_reg_req-lisence').prop('checked');

				if(form_data.name != ''){
					check_existing_name(form_data.name);
				} else {
					requrireMsg();
				}
			}

			function checkPassFields(){
				if(form_data.password != form_data.re_password){
					return false;
				} else {
					return true;
				}
			}

			function usr_name_check_response(data){
				if(data.isSuccess){
					nameMsg();
				} else {
					if (form_data.lisence){
						if ( form_data.email == '' || form_data.name == '' || form_data.password == '' || form_data.re_password == '' || form_data.phoneNo == '' || form_data.nidNo == '' || form_data.dob == '' ) {
							requrireMsg();
						} else {
							if(form_data.nidNo.trim().length == 17 || form_data.nidNo.trim().length == 13){
								if (form_data.nidorpassport) {
									if (form_data.passportNo == ''){
										requrireMsg();
									} else {
										if(checkPassFields()){
											usr_req_form_send.email = form_data.email;
											usr_req_form_send.name = form_data.name;
											usr_req_form_send.password = form_data.password;
											usr_req_form_send.passportNo = form_data.passportNo;
											usr_req_form_send.nidNo = form_data.nidNo;
											usr_req_form_send.dateofBirth = form_data.dob;
											usr_req_form_send.phoneNo = form_data.phoneNo;
											if(isValidEmailAddress(usr_req_form_send.email)){
												usr_reg_req_ajax();
											} else {
												invalidEmailMsg();
											}
										} else {
											passMsg();
										}
									}
								} else {
									if (form_data.nidNo == ''){
										requrireMsg();
									} else {
										if(checkPassFields()){
											usr_req_form_send.email = form_data.email;
											usr_req_form_send.name = form_data.name;
											usr_req_form_send.password = form_data.password;
											usr_req_form_send.nidNo = form_data.nidNo;
											usr_req_form_send.dateofBirth = form_data.dob;
											usr_req_form_send.phoneNo = form_data.phoneNo;
											if(isValidEmailAddress(usr_req_form_send.email)){
												usr_reg_req_ajax();
											} else {
												invalidEmailMsg();
											}
										} else {
											passMsg();
										}
									}
								}
							} else {
								nidlengthMsg();
							}
						}
					} else {
						lisenceMsg();
					}
				}
			}

			function check_existing_name(name){
				var check = $.ajax({
					url : "http://"+config['apiaddr']+"/check/username?name="+name,
					beforeSend: function( xhr ) {
						xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
					},
					type: 'GET',
					processData: false,
					crossDomain: true,
					contentType: "application/json",
					dataType: "json",
					data: '',
					success:function(data, textStatus, jqXHR) {
						usr_name_check_response(data);
					},
					error: function(jqXHR, textStatus, errorThrown) {
						serverMsg();
					}
				});
			}

			function usr_reg_req_ajax(){
				$.ajax({
					url : "http://"+config['apiaddr']+"/create/user",
					beforeSend: function( xhr ) {
						xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
					},
					type: 'POST',
					processData: false,
					crossDomain: true,
					data: JSON.stringify(usr_req_form_send),
					contentType: "application/json",
					dataType: "json",
					success:function(data, textStatus, jqXHR) {
						if(data.isSuccess){
							bootbox.confirm("Your Registration request has successfully submitted for approval", function(result) {
								if(result) {
									window.location.assign('login.php');
								} else {

								}
							});
						} else {
							bootbox.confirm("Your Registration request sending error", function(result) {
								if(result) {
									window.location.assign('login.php');
								} else {

								}
							});
						}
					},
					error: function(jqXHR, textStatus, errorThrown) {

					},
				}) .done(function(data) {

				});
			}

            function collect_form_data(){
				usr_reg_form_collect();
			}

			$(document).ready(function(){
				$(".reg_req_datepicker").datetimepicker({
					format: "yyyy-mm-dd",
					minView : 2,
					autoclose: true,
					todayBtn: true,
					pickerPosition: "bottom"
				});
			});

			jQuery(function($) {
				$.ajax({
					url : url+"<?php echo $baseAddress;?>api/checkLogin",
					type: "GET",
					processData: true,
					contentType: 'application/json',
					success:function(data, textStatus, jqXHR) {
						if(data.status == "success"){
							bootbox.confirm("You are still Logged-In into the system. Do you want to destroy your session?", function(result) {
								if(result) {
									window.location.assign('logout.php');
								} else {
									window.location.assign('<?php echo $baseAddress;?>');
								}
							});
						} else {

						}
					},
					error: function(jqXHR, textStatus, errorThrown) {

					},
				}) .done(function(data) {

				});

				$('#usr_reg_pass input').hide();
				$('#choose_nid_passport input').click(function(e){
					if(this.checked){
						$('#usr_reg_nid input').hide();
						$('#usr_reg_pass input').show();
					} else {
						$('#usr_reg_pass input').hide();
						$('#usr_reg_nid input').show();
					}
				});

				$('#user_register_req').click(function () {
					collect_form_data();
				});

				$(document).on('click', '.toolbar a[data-target]', function(e) {
					e.preventDefault();
					var target = $(this).data('target');
					$('.widget-box.visible').removeClass('visible');
					$(target).addClass('visible');
				});

				$(document).keyup(function(key){
					if (key.key == 'Enter') {
						var username = $('#username').val();
						var pass = $('#password').val();
						login();
					}
				});
			});

			jQuery(function($) {

				$('#btn-login-dark').on('click', function(e) {
					$('body').attr('class', 'login-layout');
					$('#id-text2').attr('class', 'white');
					$('#id-company-text').attr('class', 'blue');

					e.preventDefault();
				});
				$('#btn-login-light').on('click', function(e) {
					$('body').attr('class', 'login-layout light-login');
					$('#id-text2').attr('class', 'grey');
					$('#id-company-text').attr('class', 'blue');

					e.preventDefault();
				});
				$('#btn-login-blur').on('click', function(e) {
					$('body').attr('class', 'login-layout blur-login');
					$('#id-text2').attr('class', 'white');
					$('#id-company-text').attr('class', 'light-blue');

					e.preventDefault();
				});

			});
		</script>
	</body>
</html>
