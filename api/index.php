<?php
require 'cors_header.php';
require 'flight/Flight.php';
require 'lib/EasyPDO.php';

session_start();

Flight::route('/', function(){
    echo "Welcome to Nid Gateway";
});

Flight::route('POST /login', function(){
    $entityBody = file_get_contents('php://input');
    $data = json_decode($entityBody);

    $token = $data->token;
    $status = $data->status;

    if ($status == true){
        $_SESSION['login'] = 'YES';
        $_SESSION['user_token'] = $token;
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Logged in'
        ));
    } else {
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Invalid login or password'
        ));
    }
});

Flight::map('checkLogin', function(){
    if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {
        $output = array(
            'error' => 'Login required!'
        );
        Flight::json($output);
    }
});

Flight::route('GET /checkLogin', function(){
    if (!(isset($_SESSION['login']) && $_SESSION['login'] != '')) {
        Flight::json(array(
            'status'  => 'failure',
            'message' => 'Login Invalid'
        ));
    } else {
        Flight::json(array(
            'status'  => 'success',
            'message' => 'Logged in',
            'token' => $_SESSION['user_token']
        ));
    }
});

//FAKE CHECK LOGIN
//Flight::route('GET /checkLogin', function(){
//    Flight::json(array(
//        'status'  => 'success',
//        'message' => 'Logged in'
//    ));
//});

Flight::route('POST /saveUserData', function(){
    $entityBody = file_get_contents('php://input');
    $data = json_decode($entityBody);

    $user = $data->data;

});

Flight::route('GET /single/user/@id/@rand', function($id, $rand){
    $user_id = $id;
    Flight::json(array(
        'status'  => 'success',
        'user_id' => $user_id
    ));
});

Flight::route('GET /db_back_file_data', function(){
   $project_path = '/nid_gateway/database_backup';
   $dir = $_SERVER['DOCUMENT_ROOT'].$project_path;

   $ffs = scandir($dir, 1);
   $array = array();

   foreach($ffs as $ff){
        if($ff != '.' && $ff != '..'){
            $array[] = array(
                'name'=> $ff,
                'type'=> 'item',
                'path'=> $project_path.'/'.$ff
            );
        }
    }
   Flight::json($array);
});


Flight::start();
?>
